//
//  Line.swift
//  HuzaifahAssignment
//
//  Created by Huzaifah on 16/01/18.
//  Copyright © 2018 Huzaifah. All rights reserved.
//

import UIKit

class Line: NSObject {
    
    var startPoint: CGPoint = .zero
    var endPoint: CGPoint = .zero
    
    init(start: CGPoint, end: CGPoint) {
        startPoint = start
        endPoint = end
    }
    
}
