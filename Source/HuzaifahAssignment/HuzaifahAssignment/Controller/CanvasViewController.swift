//
//  ViewController.swift
//  HuzaifahAssignment
//
//  Created by Huzaifah on 15/01/18.
//  Copyright © 2018 Huzaifah. All rights reserved.
//

import UIKit

class CanvasViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var currentLines: [Line] = []
    var allLines: [[Line]] = []
    
    var lastPoint: CGPoint = .zero
    var swiped = false // In case when there is just a tap event.
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: self.view)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            currentLines.append(Line(start: lastPoint, end: currentPoint))
            drawLine(startPoint: lastPoint, endPoint: currentPoint)
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLine(startPoint: lastPoint, endPoint: lastPoint)
        }
        allLines.append(currentLines)
        currentLines = []
    }
    
    func drawLine(startPoint: CGPoint, endPoint: CGPoint) {
        UIGraphicsBeginImageContext(view.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            imageView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
            context.move(to: startPoint)
            context.addLine(to: endPoint)
            context.setLineCap(.round)
            context.setLineWidth(5.0)
            context.setStrokeColor(UIColor.black.cgColor)
            context.setBlendMode(.normal)
            context.strokePath()
        }
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        imageView.alpha = 1.0
        UIGraphicsEndImageContext()
        
    }
    
    @IBAction func undoAction(_ sender: Any) {
        onUndoTap()
    }
    
    @IBAction func clearAction(_ sender: Any) {
        clearAll()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        saveImage()
    }
    
    func saveImage() {
        if let theImage = getImageFromDrawnCanvas() {
            UIImageWriteToSavedPhotosAlbum(theImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    private func getImageFromDrawnCanvas() -> UIImage? {
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.image?.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func onUndoTap() {
        if allLines.count > 0 {
            allLines.removeLast()
            redrawAllLines()
        }
    }
    
    func clearAll() {
        if allLines.count > 0 {
            allLines.removeAll()
        }
        imageView.image = nil
    }
    
    func redrawAllLines() {
        imageView.image = nil
        for lines in allLines {
            for line in lines {
                drawLine(startPoint: line.startPoint, endPoint: line.endPoint)
            }
        }
    }

}

extension CanvasViewController {
    
    //MARK: Save to photos completed
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        imageView.image = nil
    }
    
}
